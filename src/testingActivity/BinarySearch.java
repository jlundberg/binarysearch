package testingActivity;

public class BinarySearch {
	
	public static int binarySearch(int[] array, int target) {
		int max = array.length - 1;
		int min = 0;
		while(min < max) {
			int middle = (int) Math.floor((max + min) / 2);
			if(array[middle] == target) {
				return middle;
			}
			if(array[middle] < target) {
				min = middle + 1;
			}else {
				max = middle - 1;
			}
			
		}
		
		return -1;
	}
}
