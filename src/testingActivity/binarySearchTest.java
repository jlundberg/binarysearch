package testingActivity;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/*
 * @Author Jared
 * @Author Hector
 */
class binarySearchTest {

	
	@Test
	void smokeTests1() {
		int[] array = {1, 3, 5, 6, 7, 9};
		assertEquals(BinarySearch.binarySearch(array, 6), 3, "Did not return 3");
	}
	
	@Test
	void smokeTests2() {
		int[] array = {1, 3, 5, 6, 7, 9};
		assertEquals(BinarySearch.binarySearch(array, 8), -1, "Did not return -1 for not found");	
	}
	
	@Test
	void smokeTests3() {
		int[] array = {1, 3, 5, 6, 7, 9};
		assertEquals(BinarySearch.binarySearch(array, 10), -1, "Did not return -1 for not found");
	}
	
	@Test
	void smokeTests4() {
		int[] array = {1, 3, 5, 6, 7, 9};
		assertEquals(BinarySearch.binarySearch(array, 0), -1, "Did not return -1 for not found");
	}
	
	@Test
	void extremeIndices1() {
		int[] array = {1, 3, 5, 6, 7, 9};
		assertEquals(BinarySearch.binarySearch(array, 9), 5, "Did not return 5");
	}
	@Test
	void extremeIndices2() {
		int[] array = {1, 3, 5, 6, 7, 9};
		assertEquals(BinarySearch.binarySearch(array, 1), 0, "Did not return 0");
	}
	
	@Test
	void extremeArraySize() {
		int[] array = new int[100000000];
		for(int i = 0; i < 100000000; i++) {
			array[i] = i;
		}
		assertEquals(BinarySearch.binarySearch(array, 3), 3, "Should have returned 3");
	}
	
	@Test
	void extremeValues() {
		int[] array = {2, 45, 99, 3064, 567899876};
		assertEquals(BinarySearch.binarySearch(array, 3064), 3, "Should have returned 3");
	}
	
	@Test
	void aroundPowersOf2() {
		int[] array = {3, 4, 6, 12, 14, 21, 24, 29, 34, 36, 38, 39, 44, 45, 46, 99};
		assertEquals(BinarySearch.binarySearch(array, 14), 4, "Should have returned 4");
	}
	
	@Test
	void emptyArray() {
		int[] array = {};
		assertEquals(BinarySearch.binarySearch(array, 3), -1, "Should have returned -1");
	}
	
}
